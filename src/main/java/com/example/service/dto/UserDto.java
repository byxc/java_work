package com.example.service.dto;

import com.example.base.BaseDTO;
import com.example.service.dto.small.DeptSmallDto;
import com.example.service.dto.small.JobSmallDto;
import com.example.service.dto.small.RoleSmallDto;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Getter
@Setter
public class UserDto extends BaseDTO implements Serializable {
    private Long id;
    private Set<RoleSmallDto> roles;
    private Set<JobSmallDto> jobs;
    private DeptSmallDto dept;
    private Long deptId;
    private String username;
    private String nickName;
    private String email;
    private String phone;

    private String gender;

    private String avatarName;
    private String avatarPath;

    @JsonIgnore  //注解@JsonIgnore的作用是“在实体类向前台返回数据时用来忽略不想传递给前台的属性或接口。”
    private String password;

    private Boolean enabled;
    @JsonIgnore
    private Boolean isAdmin = false;

    private Date pwdResetTime;
}
