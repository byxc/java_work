package com.example.service.dto;

import com.example.base.BaseDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor // build failed:java: com.example.service.dto.JobDto does not have an accessible parameterless constructor.
public class JobDto extends BaseDTO implements Serializable {
    private Long id;

    private Integer jobSort;

    private String name;

    private Boolean enabled;

    public JobDto(String name, Boolean enabled) {
        this.name = name;
        this.enabled = enabled;
    }
}
