package com.example.service.mapstruct;

import com.example.base.BaseMapper;
import com.example.domain.Role;
import com.example.service.dto.small.RoleSmallDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;


@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface RoleSmallMapper extends BaseMapper<RoleSmallDto, Role> {


}
