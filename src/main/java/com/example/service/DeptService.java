package com.example.service;

import com.example.domain.Dept;
import com.example.service.dto.DeptDto;
import com.example.service.dto.DeptQueryCriteria;

import java.util.List;
import java.util.Set;

public interface DeptService {

    List<DeptDto> queryAll(DeptQueryCriteria criteria, Boolean isQuery) throws Exception;

    List<Dept> findByPid(long pid);

    Set<Dept> findByRoleId(Long id);

    List<Long> getDeptChildren(List<Dept> deptList);

}
