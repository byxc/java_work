package com.example.service;

import com.example.service.dto.UserDto;

import java.util.List;


public interface DataService {

    List<Long> getDeptIds(UserDto user);
}

