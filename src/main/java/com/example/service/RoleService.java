package com.example.service;

import com.example.domain.Role;
import com.example.service.dto.RoleDto;
import com.example.service.dto.RoleQueryCriteria;
import com.example.service.dto.UserDto;
import com.example.service.dto.small.RoleSmallDto;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;

import java.util.List;
import java.util.Set;

public interface RoleService {
    List<RoleSmallDto> findByUsersId(Long id);
    List<GrantedAuthority> mapToGrantedAuthorities(UserDto user);
    Integer findByRoles(Set<Role> roles);
    RoleDto findById(long id);
    List<RoleDto> queryAll();
    List<RoleDto> queryAll(RoleQueryCriteria criteria);
    Object queryAll(RoleQueryCriteria criteria, Pageable pageable);

}
