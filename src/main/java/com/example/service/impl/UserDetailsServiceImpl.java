package com.example.service.impl;

import com.example.service.DataService;
import com.example.service.RoleService;
import com.example.service.UserService;
import com.example.service.dto.JwtUserDto;
import com.example.service.dto.UserDto;
//import org.apache.poi.hpsf.CustomProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
//@Service("userDetailsService") //lbtodo
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserService userService;
    @Autowired
    RoleService roleService;
    @Autowired
    DataService dataService;



    /**
     * 用户信息缓存
     *
     * @see {@link UserCacheClean}
     */
    public static Map<String, JwtUserDto> userDtoCache = new ConcurrentHashMap<>(); // lbtodo 先加上public

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDto user = userService.findByName(username);
        JwtUserDto jwtUserDto = new JwtUserDto(
                user,
                dataService.getDeptIds(user),
                roleService.mapToGrantedAuthorities(user)
        );

        return jwtUserDto;
    }
}
