package com.example.service.impl;

import com.example.domain.Dept;
import com.example.service.DataService;
import com.example.service.DeptService;
import com.example.service.RoleService;
import com.example.service.dto.UserDto;
import com.example.service.dto.small.RoleSmallDto;
import com.example.utils.enums.DataScopeEnum;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;


@Service
@RequiredArgsConstructor
public class DataServiceImpl implements DataService {
    private final RoleService roleService;
    private final DeptService deptService;

    @Override
    public List<Long> getDeptIds(UserDto user) {
        Set<Long> deptIds = new HashSet<>();

        List<RoleSmallDto> roleSet = roleService.findByUsersId(user.getId());
        // 获取对应的部门ID
        for (RoleSmallDto role : roleSet) {
            DataScopeEnum dataScopeEnum = DataScopeEnum.find(role.getDataScope());
            switch (Objects.requireNonNull(dataScopeEnum)) {
                case THIS_LEVEL:
                    deptIds.add(user.getDept().getId());
                    break;
                case CUSTOMIZE:
                    deptIds.addAll(getCustomize(deptIds,role));
                    break;
                default:
                    return new ArrayList<>(deptIds);

            }
        }

        return new ArrayList<>(deptIds);

    }

    public Set<Long> getCustomize(Set<Long> deptIds, RoleSmallDto role){
        //根据sys_roles_depts表获取当前用户角色对应的部门id
        Set<Dept> depts = deptService.findByRoleId(role.getId());
        for (Dept dept : depts) {
            deptIds.add(dept.getId());
            //若这个部门有下级部门，则获取它的下级部门
            //这样数据库表sys_roles_depts只需配最高级部门的id即可看到它下面的所有部门
            List<Dept> deptChildren = deptService.findByPid(dept.getId());
            if (deptChildren != null && deptChildren.size() != 0) {
                deptIds.addAll(deptService.getDeptChildren(deptChildren));
            }
        }
        return deptIds;
    }

}
