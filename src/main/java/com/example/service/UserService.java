package com.example.service;

import com.example.domain.User;
import com.example.service.dto.UserDto;
import com.example.service.dto.UserQueryCriteria;
import javafx.beans.binding.ObjectExpression;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public interface UserService {
    UserDto findByName(String userName);

    Object queryAll(UserQueryCriteria userQueryCriteria, Pageable pageable);

    void update(User resources) throws Exception;
    void create(User resources);
    UserDto findById(long id);
    void delete(Set<Long> ids);

}
