package com.example.service;

import com.example.service.dto.MenuDto;

import java.util.List;

public interface MenuService {
    Object buildMenus(List<MenuDto> menuDtos);

    List<MenuDto> buildTree(List<MenuDto> menuDtos);

    List<MenuDto> findByUser(Long currentUserId);

    /**
     * 懒加载菜单数据
     * @param pid /
     * @return /
     */
    List<MenuDto> getMenus(Long pid);

}
