package com.example.service;


import com.example.dao.MongoDbDao;
import com.example.entity.Hobbies;
import com.example.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MongoDbService {

    @Autowired
    private MongoDbDao mongoDbDao;

    public void save() {
        Student s=new Student();
        s.setName("lisi");
        s.setAge(17);
        s.setSex(1);
        s.setHeight(182);
        Hobbies h=new Hobbies();
        h.setHname("swing");
        s.setHobbies(h);
        mongoDbDao.save(s);
    }
    public Student get() {
        //and查询
        /* Criteria criteriaName=Criteria.where("name").is("lisi");
        Criteria criteriaAage=Criteria.where("age").is(17);
        Criteria andCriteria = new Criteria();
        andCriteria.andOperator(criteriaName,criteriaAage);
        Query query=new Query(andCriteria);*/
        //or查询
        Criteria criteriaName=Criteria.where("name").is("lisi");
        Criteria criteriaAage=Criteria.where("age").gt(16);
        Criteria orCriteria = new Criteria();
        orCriteria.orOperator(criteriaName,criteriaAage);
        Query query=new Query(orCriteria);

        return  mongoDbDao.get(query);
    }
    public List<Student> findAll() {
        return mongoDbDao.findAll();
    }
    public void update() {
        Query query=new Query(Criteria.where("name").is("zhangsan"));
        Update update=new Update();
        update.set("age",30);
        update.set("height",188);
        update.set("hobbies.hname","basketball");
        mongoDbDao.update(query,update);
    }
    public void delete() {
        Query query=new Query(Criteria.where("name").is("zhangsan"));
        mongoDbDao.delete(query);
    }





}
