package com.example.config;


import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data //https://blog.csdn.net/Alex_81D/article/details/85337730
@Component
public class RsaProperties {
    public static String privateKey;

    @Value("${rsa.private_key}")
    public void setPrivateKey(String privateKey){
        RsaProperties.privateKey = privateKey;
    }
}
