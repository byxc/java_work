package com.example.entity;

import lombok.Data;

import javax.persistence.Id;

@Data
public class Student {

    @Id
    private String id;

    private String name;
    private Integer age;
    private Integer sex;
    private Integer height;
    private Hobbies hobbies;

}
