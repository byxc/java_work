package com.example;

import com.example.utils.SpringContextHolder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Demo2Application {

    public static void main(String[] args) {
        SpringApplication.run(Demo2Application.class, args);
    }

    @RequestMapping("/hello")
    public String hello() {
        return "Hello World!";
    }


    @Bean
    public SpringContextHolder springContextHolder(){
        return new SpringContextHolder();
    }
}
