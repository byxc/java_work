package com.example.rest;


import com.example.entity.Student;
import com.example.service.MongoDbService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * https://blog.csdn.net/u014135369/article/details/112357997
 */
@RestController
@RequestMapping("/mongodb")

public class MongoDbController {
    @Autowired
    private MongoDbService mongoDbService;

    @RequestMapping("/save")
    public void save(){
        mongoDbService.save();
    }
    @RequestMapping("/get")
    public Student get(){
        return mongoDbService.get();
    }
    @RequestMapping("/findAll")
    public List<Student> findAll(){
        return mongoDbService.findAll();
    }

    @RequestMapping("/update")
    public void update(){
        mongoDbService.update();
    }
    @RequestMapping("/delete")
    public void delete() {
        mongoDbService.delete();
    }
}
