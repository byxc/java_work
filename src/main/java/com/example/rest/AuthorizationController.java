package com.example.rest;

import cn.hutool.core.util.IdUtil;
import cn.hutool.http.server.HttpServerRequest;
import com.example.annotation.rest.AnonymousDeleteMapping;
import com.example.config.RsaProperties;
import com.example.security.config.bean.LoginProperties;
import com.example.security.config.bean.SecurityProperties;
import com.example.security.security.TokenProvider;
import com.example.service.OnlineUserService;
import com.example.service.dto.AuthUserDto;
import com.example.service.dto.JwtUserDto;
import com.example.utils.RedisUtils;
import com.example.utils.RsaUtils;
import com.example.utils.SecurityUtils;
import com.wf.captcha.ArithmeticCaptcha;
import com.wf.captcha.base.Captcha;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
@Slf4j
public class AuthorizationController {
    private final RedisUtils redisUtils;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final TokenProvider tokenProvider;
    private final SecurityProperties properties;
    private final OnlineUserService onlineUserService;
    @Resource
    private LoginProperties loginProperties;
    @GetMapping(value = "/code")
    public ResponseEntity<Object> getCode() {
        // 获取运算的结果
        Captcha captcha = new ArithmeticCaptcha(111, 36);
        captcha.setLen(2);
        String uuid = "code-key-" + IdUtil.simpleUUID();
        String captchaValue = captcha.text();
        // 保存
        redisUtils.set(uuid, captchaValue, 2L, TimeUnit.MINUTES);
        // 验证码信息
        Map<String, Object> imgResult = new HashMap<String, Object>(2) {{
            put("img", captcha.toBase64());
            put("uuid", uuid);
        }};
        return ResponseEntity.ok(imgResult);
    }


    @PostMapping(value = "/login")
    public ResponseEntity<Object> login(@Validated @RequestBody AuthUserDto authUser, HttpServletRequest request) throws Exception{

        String password = RsaUtils.decryptByPrivateKey(RsaProperties.privateKey,authUser.getPassword());
        String code = (String) redisUtils.get(authUser.getUuid());

        redisUtils.del(authUser.getUuid());
        if(StringUtils.isBlank(code)){
            throw new Exception("验证码不存在或已过期");
        }
        if(StringUtils.isBlank(authUser.getCode()) || !authUser.getCode().equalsIgnoreCase(code)){
            throw new Exception("验证码错误");
        }

        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(authUser.getUsername(), password);
        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final JwtUserDto jwtUserDto = (JwtUserDto) authentication.getPrincipal();


        String token = tokenProvider.createToken(authentication);
        // 保存在线信息
        onlineUserService.save(jwtUserDto, token, request);

        Map<String,Object> authInfo = new HashMap<String,Object>(2){{
//            put("token","token");
            put("token", properties.getTokenStartWith() + token);
            put("user",jwtUserDto);
        }};
        if (loginProperties.isSingleLogin()) {
            //踢掉之前已经登录的token
            onlineUserService.checkLoginOnUser(authUser.getUsername(), token);
        }
//        System.out.println(authInfo);
        return ResponseEntity.ok(authInfo);


    }

    @ApiOperation("获取用户信息")
    @GetMapping(value = "/info")
    public ResponseEntity<Object> getUserInfo(){
        return ResponseEntity.ok(SecurityUtils.getCurrentUser());
    }

    @ApiOperation("退出登录")
    @AnonymousDeleteMapping(value = "/logout")
//    @GetMapping(value = "/logout")
    public ResponseEntity<Object> logout(HttpServletRequest request) {
        onlineUserService.logout(tokenProvider.getToken(request));
//        return new ResponseEntity<>(HttpStatus.OK);
        Map<String,Object> Info = new HashMap<String,Object>(2){{
            put("code", "0000");
            put("msg","success");
        }};
        return new ResponseEntity<>(Info,HttpStatus.OK);
    }




}
