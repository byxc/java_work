package com.example.rest;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GorunalTestContraller {
    private final static Logger logger = LoggerFactory.getLogger(GorunalTestContraller.class);

    @RequestMapping("/log")
    public String testLog(){ //日志级别按照从高到低依次为：ERROR、WARN、INFO、DEBUG
        logger.debug("====测试日志debug级别打印====");
        logger.info("====测试日志info级别打印====");
        logger.error("====测试日志error级别打印");
        logger.warn("====测试日志warn级别打印====");
        return "success";
    }
}
