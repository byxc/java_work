<h1 style="text-align: center">《Java企业级应用开发》课程大作业</h1>


#### 项目源码

|  码云   |  https://gitee.com/byxc/java_work.git   | 



#### 所用集成开发环境
- 操作系统 ： macOS
- 开发工具 ： IntelliJ IDEA
- 关系型数据库 ： MySQL
- 非关系型数据库： Redis、MongoDB


####  基础设施搭建方法
- 安装Docker
- 在Docker中搭建MySQL、Redis、MongoDB服务
- 对应修改application.yml中的MySQL、Redis、MongoDB属性配置工作
